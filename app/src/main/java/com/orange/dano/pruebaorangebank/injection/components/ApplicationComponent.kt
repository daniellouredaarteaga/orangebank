package com.orange.dano.pruebaorangebank.injection.components

import com.orange.dano.pruebaorangebank.TransactionsApplication
import com.orange.dano.pruebaorangebank.injection.modules.ActivityModule
import com.orange.dano.pruebaorangebank.injection.modules.ApplicationModule
import com.orange.dano.pruebaorangebank.injection.modules.RetrofitModule
import com.orange.dano.pruebaorangebank.injection.modules.WebServicesModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class), (RetrofitModule::class), (WebServicesModule::class)])
interface ApplicationComponent {

    fun plus(activityModule: ActivityModule): ActivityComponent

    fun inject(transactionsApplication: TransactionsApplication)

}