package com.orange.dano.pruebaorangebank.injection.modules

import com.orange.dano.data.ws.TransactionsWebServiceImpl
import com.orange.dano.data.ws.api.TransactionsApi
import com.orange.dano.domain.dataproviders.ws.TransactionsWebService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class WebServicesModule {

    private val TIMEOUT: Long = 30
    private val CONNECT_TIMEOUT: Long = 30

    @Provides
    @Singleton
    fun providesTransactionsWebService(transactionsApi: TransactionsApi): TransactionsWebService = TransactionsWebServiceImpl(transactionsApi)

    @Provides
    @Singleton
    fun providesTransactionsApi(retrofit: Retrofit): TransactionsApi = retrofit.create(TransactionsApi::class.java)
}


