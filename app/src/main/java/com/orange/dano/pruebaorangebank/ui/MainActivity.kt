package com.orange.dano.pruebaorangebank.ui

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.orange.dano.domain.model.Transaction
import com.orange.dano.pruebaorangebank.R
import com.orange.dano.pruebaorangebank.TransactionsApplication
import com.orange.dano.pruebaorangebank.amountWithCurrency
import com.orange.dano.pruebaorangebank.injection.components.ActivityComponent
import com.orange.dano.pruebaorangebank.injection.modules.ActivityModule
import com.orange.dano.pruebaorangebank.prettyDate
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : Activity(), MainUI {

    @Inject
    lateinit var mainPresenter: MainPresenter

    private val activityComponent: ActivityComponent? by lazy {
        (application as? TransactionsApplication)?.component?.plus(ActivityModule())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activityComponent?.inject(this)//TODO cosas como la inyeccion, los setView o las llamadas a los metodos del ciclo de vida del presenter deberian estar en un baseactivity o similar
        mainPresenter.view = this
        mainPresenter.onCreate()
    }

    override fun showLoading() {
        loadingView.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loadingView.visibility = View.GONE
    }

    override fun showTransactions(transactions: List<Transaction>) {
        val transaction = transactions[0]
        if (transaction.description.isNullOrBlank())
            tvLastTransactionDescription.text = getString(R.string.main_activity_no_description)
        else
            tvLastTransactionDescription.text = transaction.description
        tvLastTransactionDate.text = transaction.date.prettyDate()
        tvLastTransactionAmount.text = transaction.finalAmount.amountWithCurrency()
        transaction.finalAmount?.let {
            if (it > 0)
                tvLastTransactionAmount.background = getDrawable(R.drawable.positive_transaction_background)
            else
                tvLastTransactionAmount.background = getDrawable(R.drawable.negative_transaction_background) }
        val layoutManager = LinearLayoutManager(this)
        rvPreviousTransactions.layoutManager = layoutManager
        rvPreviousTransactions.addItemDecoration(DividerItemDecoration(this,
                layoutManager.orientation))
        rvPreviousTransactions.adapter = TransactionsAdapter(transactions.subList(1, transactions.size), this)
    }

    override fun showErrorGettingTransactions() {
        //TODO controlar error y mostrar un reintentar
    }
}
