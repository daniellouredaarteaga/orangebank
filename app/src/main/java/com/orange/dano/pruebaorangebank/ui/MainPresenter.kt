package com.orange.dano.pruebaorangebank.ui

import com.orange.dano.domain.model.Transaction
import com.orange.dano.domain.usecase.GetTransactionsUseCase
import io.reactivex.observers.DisposableSingleObserver

class MainPresenter(private val getTransactionsUseCase: GetTransactionsUseCase) {

    lateinit var view: MainUI

    fun onCreate() {
        view.showLoading()
        //TODO todos los disposables creados por CUs deberian guardarse en un composite de disposables
        //TODO y liberarlos cuando la vista muera. lo suyo es que se encaeguen las
        //TODO clases base en este caso de los presenters.
        val disposable = getTransactionsUseCase.execute(GetTransactionsSubscriber())
    }

    inner class GetTransactionsSubscriber : DisposableSingleObserver<List<Transaction>>() {

        override fun onSuccess(transactions: List<Transaction>) {
            view.hideLoading()
            view.showTransactions(transactions)
        }

        override fun onError(e: Throwable) {
            view.hideLoading()
            view.showErrorGettingTransactions()
        }

    }
}