package com.orange.dano.pruebaorangebank.injection.components

import com.orange.dano.pruebaorangebank.injection.modules.ActivityModule
import com.orange.dano.pruebaorangebank.injection.modules.PresentersModule
import com.orange.dano.pruebaorangebank.injection.modules.UseCasesModule
import com.orange.dano.pruebaorangebank.ui.MainActivity
import dagger.Subcomponent


@Subcomponent(modules = [(ActivityModule::class),
    (UseCasesModule::class),
    (PresentersModule::class)])
interface ActivityComponent {
    fun inject(mainActivity: MainActivity)
}