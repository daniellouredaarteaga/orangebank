package com.orange.dano.pruebaorangebank.injection.modules

import com.orange.dano.domain.dataproviders.ws.TransactionsWebService
import com.orange.dano.domain.usecase.GetTransactionsUseCase
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Named
import javax.inject.Singleton

@Module
class UseCasesModule {

    @Provides
    fun providesGetTransactionsUseCase(@Named("io") scheduler : Scheduler,
                                       @Named("mainThread") postExecutionScheduler : Scheduler,
                                       transactionsWebService : TransactionsWebService): GetTransactionsUseCase{
        return GetTransactionsUseCase(scheduler, postExecutionScheduler, transactionsWebService)
    }
}


