package com.orange.dano.pruebaorangebank.injection.modules

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApplicationModule {

    @Provides
    @Singleton
    @Named("io")
    fun providesIoScheduler(): Scheduler {
        return Schedulers.io()
    }

    @Provides
    @Singleton
    @Named("mainThread")
    fun providesMainThreadScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}


