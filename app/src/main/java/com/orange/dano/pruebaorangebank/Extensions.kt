package com.orange.dano.pruebaorangebank

import java.text.DateFormat
import java.text.NumberFormat
import java.util.*


fun Date?.prettyDate(): String = if (this == null) "null" else DateFormat.getDateTimeInstance().format(this)

fun Double?.amountWithCurrency(): String = if (this == null) "null" else {
    val currencyInstance = NumberFormat.getCurrencyInstance(Locale.ENGLISH)
    currencyInstance.currency = Currency.getInstance("EUR")
    currencyInstance.format(this)
}