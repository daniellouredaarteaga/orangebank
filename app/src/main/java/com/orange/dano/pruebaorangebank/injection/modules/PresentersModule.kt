package com.orange.dano.pruebaorangebank.injection.modules

import com.orange.dano.domain.usecase.GetTransactionsUseCase
import com.orange.dano.pruebaorangebank.ui.MainPresenter
import dagger.Module
import dagger.Provides

@Module
class PresentersModule {

    @Provides
    fun providesMainPresenter(getTransactionsUseCase: GetTransactionsUseCase): MainPresenter {
        return MainPresenter(getTransactionsUseCase)
    }
}


