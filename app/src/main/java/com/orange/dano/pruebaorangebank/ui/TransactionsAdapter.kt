package com.orange.dano.pruebaorangebank.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.orange.dano.domain.model.Transaction
import com.orange.dano.pruebaorangebank.R
import com.orange.dano.pruebaorangebank.amountWithCurrency
import com.orange.dano.pruebaorangebank.prettyDate
import kotlinx.android.synthetic.main.transaction_list_item.view.*

class TransactionsAdapter(private val transactions: List<Transaction>, private val context: Context) : RecyclerView.Adapter<TransactionsAdapter.TransactionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.transaction_list_item, parent, false)
        return TransactionViewHolder(v)
    }

    override fun getItemCount() = transactions.size

    override fun onBindViewHolder(viewHolder: TransactionViewHolder, position: Int) {
        val transaction = transactions[position]
        if (transaction.description.isNullOrBlank())
            viewHolder.tvDescription.text = context.getString(R.string.main_activity_no_description)
        else
            viewHolder.tvDescription.text = transaction.description
        viewHolder.tvDate.text = transaction.date.prettyDate()
        viewHolder.tvAmount.text = transaction.finalAmount.amountWithCurrency()
        transaction.finalAmount?.let {
            if (it > 0)
                viewHolder.tvAmount.background = context.getDrawable(R.drawable.positive_transaction_background)
            else
                viewHolder.tvAmount.background = context.getDrawable(R.drawable.negative_transaction_background) }
    }


    inner class TransactionViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val tvDescription: TextView = view.tvDescription
        val tvDate: TextView = view.tvDate
        val tvAmount: TextView = view.tvAmount

    }
}