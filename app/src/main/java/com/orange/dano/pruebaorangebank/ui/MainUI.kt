package com.orange.dano.pruebaorangebank.ui

import com.orange.dano.domain.model.Transaction

interface MainUI {
    fun showLoading()
    fun hideLoading()
    fun showTransactions(transactions: List<Transaction>)
    fun showErrorGettingTransactions()
}
