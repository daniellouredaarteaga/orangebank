package com.orange.dano.pruebaorangebank

import android.app.Application
import com.orange.dano.pruebaorangebank.injection.components.ApplicationComponent
import com.orange.dano.pruebaorangebank.injection.components.DaggerApplicationComponent
import com.orange.dano.pruebaorangebank.injection.modules.ApplicationModule
import com.orange.dano.pruebaorangebank.injection.modules.RetrofitModule
import com.orange.dano.pruebaorangebank.injection.modules.WebServicesModule

class TransactionsApplication : Application() {

    val component: ApplicationComponent by lazy {
        DaggerApplicationComponent
                .builder()
                .applicationModule(ApplicationModule())
                .retrofitModule(RetrofitModule())
                .webServicesModule(WebServicesModule())
                .build()
    }


    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }
}