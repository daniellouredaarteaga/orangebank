package com.orange.dano.pruebaorangebank

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.orange.dano.data.ws.TransactionsWebServiceImpl
import com.orange.dano.data.ws.api.TransactionsApi
import com.orange.dano.data.ws.model.TransactionWS
import io.reactivex.Single.just
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class TransactionsTest {

    @Mock
    private var transactionsApi: TransactionsApi? = null

    @InjectMocks
    private var transactionsWebService: TransactionsWebServiceImpl? = null

    private var gson: Gson? = null

    //TODO estaria bien extraer datos de ejemplo a un fichero aparte
    private val transactionsWithDateErrors: String = "[{\"id\":4734,\"date\":\"2018-07-11T22:49:24.000Z\",\"amount\":-193.38,\"fee\":-3.18,\"description\":\"Lorem ipsum dolor sit amet\"},{\"id\":2210,\"date\":\"2018-07-14T16:54:27.000Z\",\"amount\":165.36,\"description\":\"Est ullamco mollit ad in in proident.\"},{\"id\":1442,\"date\":\"2018-07-24T18:10:10.000Z\",\"amount\":-113.86,\"description\":\"\"},{\"id\":8396,\"date\":\"2018--11T11:31:27.000Z\",\"amount\":-153.62,\"fee\":-3.14,\"description\":\"Quis reprehenderit ullamco incididunt non ut.\"},{\"id\":3369,\"date\":\"2018-07-19T21:33:19.000Z\",\"amount\":-38.67},{\"id\":2911,\"date\":\"2018-07-29T17:56:43.000Z\",\"amount\":87.84,\"fee\":-1.11,\"description\":\"Veniam sit ut pariatur do.\"},{\"id\":2911,\"date\":\"2018-07-21T19:13:23.000Z\",\"amount\":37.74,\"fee\":0.0,\"description\":\"Lorem et incididunt aute cillum.\"},{\"id\":6595,\"date\":\"2018-07-22T13:48:48.000Z\",\"amount\":87.95,\"description\":\"Minim non sunt cupidatat magna nisi ut duis.\"},{\"id\":3371,\"date\":\"2018-07-24T21:29:11.000Z\",\"amount\":-161.56,\"fee\":-4.95,\"description\":null},{\"id\":6068,\"date\":\"2018-07-26T15:20:52.000Z\",\"amount\":92.54,\"description\":\"Nostrud laboris id officia aliquip.\"},{\"id\":5038,\"date\":\"2018-07-30T19:36:60.000Z\",\"amount\":184.98},{\"id\":6595,\"date\":\"2018-07-24T56.000Z\",\"amount\":-37.22,\"fee\":-3.99,\"description\":\"Veniam deserunt ut ullamco et ut.\"},{\"id\":2117,\"date\":\"2018-07-28T14:14:17.000Z\",\"amount\":96.56,\"description\":\"\"},{\"id\":2857,\"date\":\"07-22T13:51:12.000Z\",\"amount\":-144.63,\"fee\":-4.74,\"description\":\"Tempor dolor laboris minim cupidatat duis nisi ad.\"},{\"id\":9745,\"date\":\"2018-07-26T19:26:10.000Z\",\"amount\":166.83,\"description\":\"Fugiat elit cupidatat ipsum ad Lorem aliquip.\"}]"
    private val transactionsWithDuplicatedIds: String = "[{\n" +
            "\t\"id\": 2911,\n" +
            "\t\"date\": \"2018-07-29T17:56:43.000Z\",\n" +
            "\t\"amount\": 87.84,\n" +
            "\t\"fee\": -1.11,\n" +
            "\t\"description\": \"Veniam sit ut pariatur do.\"\n" +
            "}, {\n" +
            "\t\"id\": 2911,\n" +
            "\t\"date\": \"2018-07-21T19:13:23.000Z\",\n" +
            "\t\"amount\": 37.74,\n" +
            "\t\"fee\": 0.0,\n" +
            "\t\"description\": \"Lorem et incididunt aute cillum.\"\n" +
            "}]"

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        gson = GsonBuilder()
                .setPrettyPrinting()
                .disableHtmlEscaping()
                .serializeNulls().create()
    }

    @Test
    fun testDeletingDuplicates() {
        val listType = object : TypeToken<ArrayList<TransactionWS>>() {}.type

        `when`(transactionsApi!!.getTransactions()).thenReturn(
                just(gson!!.fromJson(transactionsWithDuplicatedIds, listType))
        )
        val transactions = transactionsWebService!!.getTransactions().blockingGet()
        assertThat(transactions).hasSize(1)
    }

    @Test
    fun testDeletingInvalidDates() {
        val listType = object : TypeToken<ArrayList<TransactionWS>>() {}.type

        `when`(transactionsApi!!.getTransactions()).thenReturn(
                just(gson!!.fromJson(transactionsWithDateErrors, listType))
        )
        val transactions = transactionsWebService!!.getTransactions().blockingGet()
        assertThat(transactions).hasSize(11)
    }

    @Test
    fun testSortingTransactions() {
        val listType = object : TypeToken<ArrayList<TransactionWS>>() {}.type

        `when`(transactionsApi!!.getTransactions()).thenReturn(
                just(gson!!.fromJson(transactionsWithDateErrors, listType))
        )
        val transactions = transactionsWebService!!.getTransactions().blockingGet()
        assertThat(transactions).isSortedAccordingTo { transaction1, transaction2 -> transaction2.date!!.compareTo(transaction1.date) }
    }
}
