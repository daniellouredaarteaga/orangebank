package com.orange.dano.domain.model

import java.util.*

data class Transaction(val id: Long?, val date: Date?, val finalAmount: Double?, val description: String?)
