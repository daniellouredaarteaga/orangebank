package com.orange.dano.domain.dataproviders.ws

import com.orange.dano.domain.model.Transaction
import io.reactivex.Single

interface TransactionsWebService {

    fun getTransactions() : Single<List<Transaction>>

}