package com.orange.dano.domain.usecase

import com.orange.dano.domain.dataproviders.ws.TransactionsWebService
import com.orange.dano.domain.model.Transaction
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver

class GetTransactionsUseCase(private val scheduler: Scheduler,
                             private val postExecutionScheduler: Scheduler,
                             private val transactionsWebService: TransactionsWebService) {


    //TODO Toda la infraestructura de casos de uso deberia estar en clases base de forma que
    //TODO en este CU solo tengas el metodo buildSingle. Lo mismo ocurriria si tuvieras que
    //TODO hacer CUs Maybe, Completable...
    fun execute(subscriber: DisposableSingleObserver<List<Transaction>>): Disposable {
        return single()
                .subscribeWith(subscriber)
    }

    private fun single(): Single<List<Transaction>> {
        return buildSingle()
                .compose { it.subscribeOn(scheduler).observeOn(postExecutionScheduler) }
    }

    private fun buildSingle(): Single<List<Transaction>> {
        return transactionsWebService.getTransactions()
    }
}