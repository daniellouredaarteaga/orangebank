package com.orange.dano.data.ws

import com.orange.dano.data.ws.api.TransactionsApi
import com.orange.dano.data.ws.model.mapper.TransactionMapper
import com.orange.dano.domain.dataproviders.ws.TransactionsWebService
import com.orange.dano.domain.model.Transaction
import io.reactivex.Single

class TransactionsWebServiceImpl(private val transactionsApi: TransactionsApi) : TransactionsWebService {

    override fun getTransactions(): Single<List<Transaction>> {
        return transactionsApi.getTransactions()
                .map(TransactionMapper().transformList)
    }
}

