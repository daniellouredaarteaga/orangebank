package com.orange.dano.data.ws.model.mapper

import com.orange.dano.data.ws.model.TransactionWS
import com.orange.dano.domain.model.Transaction
import java.text.ParseException
import java.text.SimpleDateFormat

class TransactionMapper {

    val transformList: (List<TransactionWS>) -> List<Transaction> = {
        it.map {
            Transaction(it.id, try {
                //TODO habria que valorar si la transaccion de id 5038 y fecha '2018-07-30T19:36:60.000Z' es correcta o no
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(it.date)
            } catch (e: ParseException) {
                null
            }, it.fee?.let { fee ->
                it.amount?.plus(fee)
            } ?: it.amount, it.description)
        }.filter { it.date != null }
                .sortedByDescending { it.date }
                .distinctBy { it.id }
    }

}
