package com.orange.dano.data.ws.api

import com.orange.dano.data.ws.model.TransactionWS
import io.reactivex.Single
import retrofit2.http.GET

interface TransactionsApi {

    @GET("bins/1a30k8")
    fun getTransactions(): Single<List<TransactionWS>>
}