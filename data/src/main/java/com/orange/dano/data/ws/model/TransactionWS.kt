package com.orange.dano.data.ws.model

data class TransactionWS(val id: Long?, val date: String?, val amount: Double?, val fee: Double?, val description: String?)